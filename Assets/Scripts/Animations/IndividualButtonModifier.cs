﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;

public class IndividualButtonModifier : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private Vector3 originalPos;
    private Vector3 originalScale;
    // Start is called before the first frame update
    void Start()
    {
        originalPos = this.transform.position;
        originalScale = this.transform.localScale;
    }

    private void OnDisable()
    {
        AnimateItemScaleAndMove(false);
    }
    // Update is called once per frame
    void Update()
    {
    }

    private void AnimateItemScaleAndMove(bool zoom)
    {
        GameObject b = this.gameObject;
        GameObject line = b.transform.GetChild(0).gameObject;

        if (zoom)
        {
    //        b.transform.DOScale(originalScale + (Vector3.one * 0.12f), 0.1f);
      //      b.transform.DOMove(new Vector3(originalPos.x + 60f, originalPos.y, originalPos.z), 0.4f);
            line.SetActive(true);
        }
        else
        {
       //     b.transform.DOScale(originalScale, 0.1f);
      //      b.transform.DOMove(originalPos, 0.4f);
            line.SetActive(false);
        }
    }

    private void ScaleItem(bool zoom, float value)
    {
        GameObject b = this.gameObject;
        GameObject line = b.transform.GetChild(0).gameObject;

        if (zoom)
        {
            b.transform.DOScale(originalScale + (Vector3.one * value), 0.1f);
   //         b.transform.DOMove(new Vector3(originalPos.x + 60f, originalPos.y, originalPos.z), 0.4f);
            line.SetActive(true);
        }
        else
        {
            b.transform.DOScale(originalScale, 0.1f);
        //    b.transform.DOMove(originalPos, 0.4f);
            line.SetActive(false);
        }
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        AnimateItemScaleAndMove(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        AnimateItemScaleAndMove(false);
    }



}
