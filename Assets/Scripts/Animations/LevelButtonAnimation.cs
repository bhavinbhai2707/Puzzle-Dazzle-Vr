﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class LevelButtonAnimation : MonoBehaviour
{
    private Vector3 originalPos;
    private Vector3 originalScale;
    public TextMeshProUGUI levelNameT;
    public GameObject outerCircle;

    public GameObject levelSelectScreen, difficultySelectScreen, buttonHolder;

    public string levelName;

    public bool enableThis = false;
    // Start is called before the first frame update
    void Start()
    {
        originalPos = this.transform.position;
        originalScale = this.transform.localScale;
        if (enableThis)
            EnableBubble();
    }

    private void ScaleItem(bool zoom, float value)
    {
        GameObject b = this.gameObject;
        GameObject line = b.transform.GetChild(0).gameObject;

        if (zoom)
        {
    //        b.transform.DOScale(originalScale + (Vector3.one * value), 0.1f);
            //         b.transform.DOMove(new Vector3(originalPos.x + 60f, originalPos.y, originalPos.z), 0.4f);
            line.SetActive(true);
        }
        else
        {
     //       b.transform.DOScale(originalScale, 0.1f);
            //    b.transform.DOMove(originalPos, 0.4f);
            line.SetActive(false);
        }
    }

    private void OnMouseEnter()
    {

        EnableBubble();
    }

    public void EnableBubble()
    {
        if (outerCircle != null)
        {
            //  outerCircle.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
            outerCircle.gameObject.SetActive(true);
        }
        ScaleItem(true, 1f);
        levelNameT.text = levelName;
        UIHandler.instance.levelNameToLoad = levelName;
        UIHandler.instance.mainmenuHighScore.text = PlayerPrefs.GetInt(levelName + "hs", 0).ToString();
    }

    private void OnMouseDown()
    {
       // levelSelectScreen.SetActive(false);
       // difficultySelectScreen.SetActive(true);
       // buttonHolder.SetActive(false);
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
    }

    private void OnMouseExit()
    {
        ScaleItem(false, 1f);
        if (outerCircle != null)
        {
            //    outerCircle.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            outerCircle.gameObject.SetActive(false);
        }
    }

}
