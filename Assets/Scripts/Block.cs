﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    Material mat;
    bool alter = false;
    float value = 0;

    private void Start()
    {
        mat = this.GetComponent<Renderer>().material;

        int rand = Random.Range(0, 2);
        if (rand == 0)
            alter = true;
        else
            alter = false;
    }


    private void Update()
    {
        if (LevelUniqueIdentifier.instance.animateToMusic)
        {
            if (AudioPeer.amplitudeBuffer > 0.4f)
                value = AudioPeer.amplitudeBuffer * 2;
            else
                value = 1.5f;

            /*   if(alter)
               {
                   if (AudioPeer.amplitudeBuffer < 0.8f)
                       value = AudioPeer.amplitudeBuffer * 3;
                   else
                       value = 1.5f;
               }
               else
               {
                   if (AudioPeer.amplitudeBuffer > 0.8f)
                       value = AudioPeer.amplitudeBuffer * 2;
                   else
                       value = 2;
               }

               mat.SetFloat("Vector1_32392080", value);*/
            mat.SetFloat("Vector1_32392080", value);
        }
    }
}
