﻿
[System.Serializable]
public class Combinations 
{
    public int[] color;
    public int amount;

    public Combinations(int[] color, int amount)
    {
        this.color = color;
        this.amount = amount;
    }
}
