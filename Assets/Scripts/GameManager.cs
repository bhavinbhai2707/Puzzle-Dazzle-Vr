﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Analytics;

public enum GameOrientation
{
    landscape = 0,
    portrait = 1
}
 public enum GameMode
{
    Arcade = 0,
    Survival = 1

}

public enum GameDifficulty
{
    Easy = 0,
    Medium = 1,
    Hard = 2
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public bool isGameover = false;
    public GameOrientation gameOrientation;
    public GameMode gameMode;
    public GameDifficulty gameDifficulty;
    public bool isSurvival = true;


    public bool spawnPiece = false;

    public GameObject currentPiece;

    public GameObject[] pieceControllerObj;

    public float customTimeScale = 1f;

    public TMP_Dropdown dropdown;

    public GameObject controlButtons;
    public bool isSwipingEnabled = false;
    private float fallSpeed = 0.8f;

    public GameObject Effects;

    public Scene intoTheOcenaScene, magicOrbScene, snowFallScene;


  

    public float FallSpeed
    {
        get
        {
            return fallSpeed;
        }
        set
        {
            fallSpeed = value;
        }
    }

    private int coins = 0;

    public int Coins { get { return coins; } set { coins = value; } }

    private void Start()
    {
 

        UIHandler.instance.UpdateScoreUI();
        coins = PlayerPrefs.GetInt("coins", 0);

        UIHandler.instance.UpdateScoreUI();

        if(SceneManager.GetActiveScene().name == "Menu")
        ControlDropDown();

        Analytics.CustomEvent("Started Level", new Dictionary<string, object>
        {
            {"Level Name", SceneManager.GetActiveScene().name}
        });

    }

    private IEnumerator DelayEffects()
    {
        yield return new WaitForSeconds(1f);
        Effects.SetActive(true);
    }
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }

        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIHandler.instance.OnPressedPause();
        }


        if (Input.GetKeyDown(KeyCode.Z))
        {
            FindObjectOfType<AudioHandler>().mainAudioSource.time = FindObjectOfType<AudioHandler>().mainAudioSource.clip.length - 3;
        }

        if (FindObjectOfType<LevelUniqueIdentifier>().songcompleted)
        {
            /*   int level = SceneManager.GetActiveScene().buildIndex;

               level++;

               if (level > SceneManager.sceneCountInBuildSettings)
                   level = 3;

               Debug.LogError("SF " + SceneManager.sceneCount);
               SceneManager.LoadScene(level);*/

            PlayerPrefs.SetInt("levelsCompleted_" + SceneManager.GetActiveScene().name, 1);

            Analytics.CustomEvent("Completed Level", new Dictionary<string, object>
                {
                    {"Level Name", SceneManager.GetActiveScene().name}
                });

            Analytics.CustomEvent("Score Won Level", new Dictionary<string, object>
                {
                    {"Level Name", SceneManager.GetActiveScene().name},
                    {"Score", ScoreManager.instance.score}
                });

            int l1 = PlayerPrefs.GetInt("levelsCompleted_Into The Ocean", 0);
            int l2 = PlayerPrefs.GetInt("levelsCompleted_Magic Orb", 0);
            int l3 = PlayerPrefs.GetInt("levelsCompleted_Snowfall", 0);

            if (l1 == 0)
                SceneManager.LoadScene("Into The Ocean");
            else if (l2 == 0)
                SceneManager.LoadScene("Magic Orb");
            else if (l3 == 0)
                SceneManager.LoadScene("Snowfall");
            else
                SceneManager.LoadScene("Menu");


        }
        else if (FindObjectOfType<LevelUniqueIdentifier>().songcompleted && !FindObjectOfType<LevelUniqueIdentifier>().targetAchieved)
        {
            ScoreManager.instance.SaveScore();
            AudioHandler.Instance.FadeAudio();
            GameObject.Find("falling sound").GetComponent<AudioSource>().clip = AudioHandler.Instance.gameOverClip;
            GameObject.Find("falling sound").GetComponent<AudioSource>().Play();
            FindObjectOfType<TrailHead>().StopAllCoroutines();
            UIHandler.instance.ShowResult();
        }
    }

    private void ShowResult()
    {
        //UIHandler.instance.ShowResult();
        Instantiate(Resources.Load("Effects") as GameObject);
    }

    public void OnPressedLeft()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(1);
    }

    public void OnPressedRight()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(0);
    }

    public void OnPressedFlip()
    {
        if (currentPiece == null)
            return;

        currentPiece.GetComponent<PieceController>().ExecuteCommandBlock(3);
    }

    public void ControlDropDown()
    {
        if(dropdown.value == 0)
        {
            isSwipingEnabled = true;
            controlButtons.SetActive(false);
        }
        else if(dropdown.value == 1)
        {
            isSwipingEnabled = false;
            controlButtons.SetActive(true);
        }
    }

    public void UpdateCoins(int amount)
    {
        Coins += amount;
        UIHandler.instance.UpdateScoreUI();
    }

    private void SetGameMode()
    {
        switch(PlayerPrefs.GetInt("gamemode"))
        {
            case 0:
                gameMode = GameMode.Arcade;
                break;
            case 1:
                gameMode = GameMode.Survival;
                break;
            default:
                break;
        }
    }

    public void SetGameDifficulty()
    {
        switch (PlayerPrefs.GetInt("gamedifficulty"))
        {
            case 0:
                gameDifficulty = GameDifficulty.Easy;
                break;
            case 1:
                gameDifficulty = GameDifficulty.Medium; 
                break;
            case 2:
                gameDifficulty = GameDifficulty.Hard;
                break;
            default:
                break;
        }
    }

    public IEnumerator TempSlowMotion(float seconds, float slowValue)
    {
        Time.timeScale = slowValue;
        yield return new WaitForSeconds(seconds);
        Time.timeScale = 1;
    }
}
