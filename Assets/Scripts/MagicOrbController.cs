﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class MagicOrbController : MonoBehaviour
{

    public VisualEffect vfxObj;

    private void Update()
    {
        float size = 1 * (AudioPeer.amplitude * 2);
        vfxObj.SetFloat("size", size);
    }
}
