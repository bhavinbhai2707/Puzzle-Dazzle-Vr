﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMechanic : MonoBehaviour
{
    public List<GameObject> spawnPieces;
    public Transform[] ExtraPiecesTransform;
    public Sprite[] piece_Tex;
    public Material[] mats;
    public Sprite[] currentPieceTex;

    public string pieceName = "Piece_Square";

    public List<Combinations> combinationsEasy, combinationsMedium, combinationsHard;

    private void Start()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "IntoTheOcean")
        {
            pieceName = "Piece_Square_Underwater";
        }
        else
        {
            pieceName = "Piece_Square";
        }

      

        int index = PlayerPrefs.GetInt("theme", 0);
        currentPieceTex = new Sprite[2];
        currentPieceTex[0] = piece_Tex[index * 2];
        currentPieceTex[1] = piece_Tex[(index * 2) + 1];
        spawnPieces = new List<GameObject>();

        if (LevelUniqueIdentifier.instance.animateToMusic)
        {

            mats[0].SetColor("Color_48F7F441", LevelUniqueIdentifier.instance.blockColor1);
            mats[1].SetColor("Color_48F7F441", LevelUniqueIdentifier.instance.blockColor2);
            mats[0].SetColor("Color_D18141CE", LevelUniqueIdentifier.instance.blockColor1);
            mats[1].SetColor("Color_D18141CE", LevelUniqueIdentifier.instance.blockColor2);
        }
        else
        {

           LevelUniqueIdentifier.instance.basicMat1.color = LevelUniqueIdentifier.instance.blockColor1;
            LevelUniqueIdentifier.instance.basicMat2.color = LevelUniqueIdentifier.instance.blockColor2;
            mats[0] = LevelUniqueIdentifier.instance.basicMat1;
            mats[1] = LevelUniqueIdentifier.instance.basicMat2;
            mats[0].SetColor("_EmissionColor", LevelUniqueIdentifier.instance.blockColor1);
            mats[1].SetColor("_EmissionColor", LevelUniqueIdentifier.instance.blockColor2);

        }

        GameManager.instance.SetGameDifficulty();

        SpawnPreProgrammedPieces();

       /* for (int i = 0; i < 4; i++)
        {
            SpawnPiece();
            if (i > 0)
                spawnPieces[i].GetComponent<PieceController>().enabled = false;
        }*/
        ReArrangeExtraPieces();
    }


    public void SpawnPiece()
    {
        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;
            GameManager.instance.currentPiece.transform.rotation = Quaternion.identity;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);

            if (LevelUniqueIdentifier.instance.fallingParticle != null)
            {
                if(GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle != null)
                GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle.SetActive(true);
            }
        }

        if (spawnPieces.Count <= 3)
            SpawnPreProgrammedPieces();

        if (spawnPieces.Count > 0)
            ReArrangeExtraPieces();

        GameManager.instance.spawnPiece = false;
    }

    private void SpawnPreProgrammedPieces()
    {

        #region setting difficulty
        List<Combinations> currentCombination = null;
        if (GameManager.instance.gameDifficulty == GameDifficulty.Easy)
            currentCombination = combinationsEasy;
        else if (GameManager.instance.gameDifficulty == GameDifficulty.Medium)
            currentCombination = combinationsMedium;
        else if (GameManager.instance.gameDifficulty == GameDifficulty.Hard)
            currentCombination = combinationsHard;
        #endregion

        #region calculating amount
        int amount = TotalAmountInASet(currentCombination);
        #endregion



        #region spawning the Set


        string tag1, tag2;
        GameObject go = null;
        List<GameObject> tempSpawnPieces = new List<GameObject>();

        foreach (Combinations c in currentCombination)
        {

            for (int i = 0; i < c.amount; i++)
            {
                int random = Random.Range(0, 2);
                if (random == 0)
                {
                    tag1 = "Blue";
                    tag2 = "Yellow";
                }
                else
                {
                    tag1 = "Yellow";
                    tag2 = "Blue";
                }

                if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                    go = Instantiate(Resources.Load(pieceName) as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.65f, GridSystem.instance.height + 0.7f, 0), Quaternion.identity, transform);
                else
                    go = Instantiate(Resources.Load(pieceName) as GameObject, new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0), Quaternion.identity, transform);
                go.GetComponent<PieceController>().enabled = false;

                if (c.color.Length == 2)
                {
                    GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
                    GameObject.Destroy(go.transform.GetChild(go.transform.childCount - 2).gameObject);
                }


                for (int j = 0; j < c.color.Length; j++)
                {
                    if (c.color[j] == 0)
                    {
                        SetPiece(go.transform.GetChild(j).gameObject, tag1);
                    }
                    else
                    {
                        SetPiece(go.transform.GetChild(j).gameObject, tag2);
                    }
                }


                if (LevelUniqueIdentifier.instance.fallingParticle != null)
                {
                    GameObject fallParticle = Instantiate(LevelUniqueIdentifier.instance.fallingParticle as GameObject);
                    go.GetComponent<PieceController>().fallingParticle = fallParticle;
                    fallParticle.name = "fallingParticle";
                    fallParticle.SetActive(false);
                    fallParticle.transform.position = go.transform.position;
                }
                tempSpawnPieces.Add(go);

                if (tempSpawnPieces.Count >= 4)
                    ReArrangeExtraPieces();
                GameManager.instance.spawnPiece = false;
            }
        }

        UtilityHelper.Shuffle(tempSpawnPieces);

        foreach(GameObject g in tempSpawnPieces)
        {
            spawnPieces.Add(g);
        }

        tempSpawnPieces.Clear();

        if (spawnPieces.Count != 0)
        {
            if (GameManager.instance.currentPiece == null)
                spawnPieces[0].GetComponent<PieceController>().enabled = true;
            GameManager.instance.currentPiece = spawnPieces[0].gameObject;
            GameManager.instance.currentPiece.transform.rotation = Quaternion.identity;

            if (GameManager.instance.gameOrientation == GameOrientation.landscape)
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.65f, GridSystem.instance.height + 0.7f, 0);
            else
                spawnPieces[0].transform.position = new Vector3(GridSystem.instance.width / 2 + 0.6f, GridSystem.instance.height + 0.79f, 0);

            if (LevelUniqueIdentifier.instance.fallingParticle != null)
            {
                if (GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle != null)
                    GameManager.instance.currentPiece.GetComponent<PieceController>().fallingParticle.SetActive(true);
            }
        }
        #endregion
    }

    public void ReArrangeExtraPieces()
    {
        for(int i = 1; i < spawnPieces.Count; i++)
        {
            if (i >= ExtraPiecesTransform.Length)
            {
                spawnPieces[i].transform.position = ExtraPiecesTransform[ExtraPiecesTransform.Length - 1].position;
                ExtraPiecesTransform[ExtraPiecesTransform.Length - 1].transform.LookAt(Camera.main.transform);
                spawnPieces[i].transform.LookAt(Camera.main.transform);
            }
            else
            {
                spawnPieces[i].transform.position = ExtraPiecesTransform[i - 1].position;
                ExtraPiecesTransform[i - 1].transform.LookAt(Camera.main.transform);
                spawnPieces[i].transform.LookAt(Camera.main.transform);
            }
        }
    }

    private void SetPiece(GameObject go, string tag)
    {
        go.tag = tag;
        int rand = 0;
        if (tag == "Blue")
            rand = 0;
        else if (tag == "Yellow")
            rand = 1;
        go.GetComponent<Renderer>().material = mats[rand];
    }

    private int TotalAmountInASet(List<Combinations> currentCombination)
    {
        int amount = 0;
        foreach (Combinations c in currentCombination)
        {
            amount += c.amount;
        }
        return amount;
    }
}
