﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
public class SplashScreen : MonoBehaviour
{
    private void OnEnable()
    {
        Analytics.CustomEvent("Started Game");
        StartCoroutine(LoadwithDelay());
    }

    private IEnumerator LoadwithDelay()
    {
        yield return new WaitForSeconds(15f);
        SceneManager.LoadScene("Menu");
    }

    public void LoadLevel(string level)
    {
        SceneManager.LoadScene(level);
    }
}
