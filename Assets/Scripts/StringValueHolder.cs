﻿public static class StringValueHolder
{
    public static string musicVolumePrefs = "music_volume";
    public static string sfxVolumePrefs = "sfx_volume";
    public static string fullscreenPrefs = "fullscreen";
    public static string antiAliasingPrefs = "antiAliasing";
}
